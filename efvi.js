// selecciona los elementos tipo desplegable y menú y los oculta al inicio
var elemento = document.querySelectorAll('[data-efvi-desplegable] , [data-efvi-menu]');
for (x=0; x < elemento.length; x++)
	efviOcultar(elemento[x]);

// selecciona los elementos que actúan de "interruptor" y agrega el evento
var elemento = document.querySelectorAll('[data-efvi-desplegar], [data-efvi-plegar]');
for (x=0; x < elemento.length; x++)
	elemento[x].addEventListener ("click",  function (ev) { efviIntercambiar(this); ev.preventDefault(); return false; } );
	
// selecciona los enlaces de menú para cerrarlo cuando son clickeados
var elemento = document.querySelectorAll('[data-efvi-menu]');
for (x=0; x < elemento.length; x++) {
	var enlace = elemento[x].querySelectorAll('a:not([href="#"])');
	var entorno = this;
	for (y=0; y < enlace.length; y++) 
		enlace[y].addEventListener ("click",  function () { efviOcultarPadre(this); } );
}
	

// oculta o muestra un elemento 
function efviIntercambiar(elemento) {
	var atributo = elemento.getAttribute('data-efvi-desplegar');
	if (atributo == null)
		var atributo = elemento.getAttribute('data-efvi-plegar');
	
	var atributo = '[data-efvi-desplegable='+ atributo +'] , [data-efvi-plegable='+ atributo +']  , [data-efvi-menu='+ atributo +']';
	var objetivo = document.querySelectorAll(atributo);
	for (x=0; x < objetivo.length; x++)
		if (objetivo[x].getAttribute('data-efvi-visibilidad') == 'oculto')
			efviMostrar(objetivo[x]);
		else
			efviOcultar(objetivo[x]);
	
}

function efviOcultar(elemento) {
	// verifica si tiene una clase personalizada
	var clasepersonalizada = elemento.getAttribute('data-efvi-clase');
	
	if (clasepersonalizada != null) {
	// hace una copia del atributo class
		var clase = elemento.getAttribute('class');
		if (clase== null)
			clase='';
		elemento.setAttribute('data-efvi-copiaclase', clase);
		elemento.setAttribute('class', clase + ' ' + clasepersonalizada);
	}
	elemento.setAttribute('data-efvi-visibilidad', 'oculto' );
}

function efviMostrar(elemento) {
	// verifica si hay una copia de la clase
	var clasecopiada = elemento.getAttribute('data-efvi-copiaclase');
	
	// recupera la copia de la clase
	if (clasecopiada != null)
		elemento.setAttribute('class', clasecopiada);
	
	elemento.setAttribute('data-efvi-visibilidad', 'visible' );
}

// se usa para ocultar elementos del tipo Menú
function efviOcultarPadre (elemento) {
	while (elemento.tagName != 'body' && elemento.tagName != 'html' && elemento != null) {
		if (elemento.getAttribute('data-efvi-menu') != null) {
			efviOcultar(elemento);
			return;
		}
		
		elemento = elemento.parentNode;
	}
}

