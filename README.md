# EfVi

Este programa sirve para mostrar y ocultar elementos de un documento HTML mediante el uso de javascript y CSS.
Concretamente a un elemento le quita una clase CSS que lo hace visible por una que lo convierte en invisible, y viceversa, 
el efecto de la transición queda a cargo exclusivamente del CSS.



# Características

* No requiere jQuery.
* No requiere boostrap.
* No requiere complementos externos.
* No utiliza Apis de sitios externos.
* No utiliza código propietario.
* Permite utilizar clases personalizadas.
* Utiliza etiquetas DATA- para conservar la validez del HTML
* Es LIBRE.



# Instalación
Requiere dos archivos, el archivo javascript y el archivo de estilos.

El en documento entre las etiquetas HEAD incluir el CSS.
<link rel='stylesheet' type='text/css' href='efvi.css' />

Al final del documento incluir el javascript.
<script type="text/javascript" src="efvi.js"></script>


# Sintaxis

Identificar elementos plegables (ocultables)
data-efvi-plegable = <nombre del elemento plegable>

Identificar un elemento que al hacer click muestre u oculte un elemento plegable
data-efvi-plegar = <nombre del elemento plegable que se desea plegar>


# Ejemplo

<h3 data-efvi-plegar="ejPlegable">Ejemplo Plegable y con CSS Personalizado</h3>
<p data-efvi-plegable="ejPlegable">
	Soy un Texto Plegable :P
</p>


# Más atributos

data-efvi-desplegable -> nombra un elemento desplegable (lo oculta al principio).
data-efvi-plegable -> nombra un elemento plegable (lo deja visible al principio).
data-efvi-menu -> nombra un elemento menú (lo oculta cuando hacen clicks en sus enlaces).

data-efvi-plegar y data-efvi-desplegar -> muestra u oculta un elemento desplegable, plegable o menú.

data-efvi-clase -> clase que obtendrá el elemento cuando pase a modo oculto.




# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



